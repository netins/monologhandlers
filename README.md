# MonologHandlers

## Using as service in symfony

In your config.yml:

```
slack_handler:
    class: Netins\MonologHandlers\Handler\SlackHandler
    arguments:
        - '%logger.slack.channel%'
        - '%logger.slack.token%'
        - '%logger.slack.bot_name%'
        - '%logger.slack.level%'
        - '%logger.slack.icon_emoji%'
        - '%logger.slack.bot_emoji%'
        - '@logger'
```         

#### Parameters description:

logger.slack.channel (string, required) - name of slack channel
logger.slack.token (string, required) - slack token
logger.slack.bot_name (string, required) - bot name
logger.slack.level (int, optional) - minimal logging level (https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md)
logger.slack.icon_emoji (string) - emoji icon on the beginning of the log
logger.slack.bot_emoji (string) - bot's avatar icon
@logger (Psr/Log/LoggerInterface) - optional logger service in case of problems

#### parameters.yml example:
```
logger.slack.channel: '#log-sandbox'
logger.slack.token: xoxb-fooooooooooo-baaaaaaaaaaaaar
logger.slack.bot_name: Gelbot
logger.slack.icon_emoji: ':bangbang:'
logger.slack.bot_emoji: ':snoopdance:'
logger.slack.level: 400
```

#### monolog config

```
monolog:
    handlers:
        slack_handler:
            type: service
            id: slack_handler
```