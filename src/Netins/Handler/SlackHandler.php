<?php

namespace Netins\MonologHandlers\Handler;

use GuzzleHttp\Psr7\Request;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use GuzzleHttp\ClientInterface;

class SlackHandler extends AbstractProcessingHandler
{
    const SLACK_API_URL = 'https://slack.com/api/chat.postMessage';

    /**
     * @var string
     */
    private $channel;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $botName;

    /**
     * @var string
     */
    private $iconEmoji;

    /**
     * @var string
     */
    private $botEmoji;

    /**
     * @var
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger = null;

    /**
     * SlackHandler constructor.
     * @param ClientInterface $httpClient
     * @param string $channel
     * @param string $token
     * @param string $botName
     * @param int $level
     * @param string $iconEmoji
     * @param string $botEmoji
     */
    public function __construct(
        ClientInterface $httpClient,
        $channel,
        $token,
        $botName,
        $level = Logger::ERROR,
        $iconEmoji = ':bangbang:',
        $botEmoji = ':snoopdance:'
    )
    {
        parent::__construct($level, true);

        $this->httpClient = $httpClient;
        $this->channel = $channel;
        $this->token = $token;
        $this->botName = $botName;
        $this->iconEmoji = $iconEmoji;
        $this->botEmoji = $botEmoji;
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        if (!isset($record['context']['exception'])) {
            return;
        }

        /** @var \Exception $exception */
        $exception = $record['context']['exception'];

        if ($exception instanceof NotFoundHttpException || $exception instanceof AccessDeniedHttpException) {
            return;
        }

        try {
            $message = $this->iconEmoji . ' ' . $record['message'] . PHP_EOL . PHP_EOL;
            $message .= ':zap: trace:' . PHP_EOL . PHP_EOL . $exception->getTraceAsString();

            $this->httpClient->request('POST', self::SLACK_API_URL, [
                'form_params' => [
                    'token' => $this->token,
                    'channel' => $this->channel,
                    'text' => $message,
                    'username' => $this->botName,
                    'icon_emoji' => $this->botEmoji,
                ]
            ]);
        } catch (\Exception $e) {
            if ($this->logger instanceof LoggerInterface) {
                $this->logger->critical('Slack handler failed to process the exception', [
                    'message' => $e->getMessage(),
                    'originalMessage' => $exception->getMessage(),
                ]);
            }
            return;
        }
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}