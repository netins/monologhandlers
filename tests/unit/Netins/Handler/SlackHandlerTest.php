<?php

namespace Tests\Unit\Netins\Handler\SlackHandlerTest;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use Netins\MonologHandlers\Handler\SlackHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class SlackHandlerTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Client
     */
    protected $clientMock;

    /**
     * @var SlackHandler
     */
    protected $handler;

    public function setUp()
    {
        $this->clientMock = $this->getMockBuilder(Client::class)->disableOriginalConstructor()->getMock();
        $this->handler = new SlackHandler(
            $this->clientMock,
            '#log-sandbox',
            'xoxb-536665845606-token',
            'Gelbot',
            Logger::ERROR
        );
    }

    /**
     * @test
     */
    public function handlerWillNotCallApiWhenExceptionIsTypeOfAccessDenied()
    {
        $errorMessage = 'Wystpil blad';

        $record = [
            'level' => Logger::ERROR,
            'context' => [
                'exception' => new AccessDeniedHttpException($errorMessage),
            ],
            'extra' => [],
            'message' => $errorMessage,
        ];

        $this->clientMock->expects($this->never())
            ->method('request');

        $this->handler->handle($record);
    }

    /**
     * @test
     */
    public function handlerWillNotCallApiWhenExceptionIsTypeOfNotFound()
    {
        $errorMessage = 'Wystpil blad';

        $record = [
            'level' => Logger::ERROR,
            'context' => [
                'exception' => new NotFoundHttpException($errorMessage),
            ],
            'extra' => [],
            'message' => $errorMessage,
        ];

        $this->clientMock->expects($this->never())
            ->method('request');

        $this->handler->handle($record);
    }

    /**
     * @test
     */
    public function handlerWillNotCallApiWhenThereIsNoExceptionInRecord()
    {
        $errorMessage = 'Wystpil blad';

        $record = [
            'level' => Logger::ERROR,
            'context' => [
                'exception' => null,
            ],
            'extra' => [],
            'message' => $errorMessage,
        ];

        $this->clientMock->expects($this->never())
            ->method('request');

        $this->handler->handle($record);
    }

    /**
     * @test
     */
    public function handlerWillCallApiWhenRecordContainsGenericException()
    {
        $errorMessage = 'Wystpil blad';

        $record = [
            'level' => Logger::ERROR,
            'context' => [
                'exception' => new \Exception(),
            ],
            'extra' => [],
            'message' => $errorMessage,
        ];

        $this->clientMock->expects($this->once())
            ->method('request');

        $this->handler->handle($record);
    }

    /**
     * @test
     */
    public function handlerWillUseLoggerWhenHttpClientThrowsAnException()
    {
        $loggerMock = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $this->handler->setLogger($loggerMock);

        $errorMessage = 'Wystpil blad';

        $record = [
            'level' => Logger::ERROR,
            'context' => [
                'exception' => new \Exception(),
            ],
            'extra' => [],
            'message' => $errorMessage,
        ];

        $this->clientMock->expects($this->once())
            ->method('request')
            ->willThrowException(new \Exception());

        $loggerMock->expects($this->once())
            ->method('critical');

        $this->handler->handle($record);
    }
}