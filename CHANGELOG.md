## Changelog

## [0.2.0] - 2019-10-17
### Added
* curl replaced with guzzle http
* 100% code coverage

## [0.1.1] - 2019-10-14
### first stable version